import * as constants from '../constants';
import { Categories } from '../components/pages';
import Layout from '../components/organisms/layout/layout.organism';

export default [
	{
		path: constants.routes.categories.LIST,
		component: Categories,
		useLayout: true,
		layout: Layout,
		props: {},
		requireLogin: true,
	},
];
