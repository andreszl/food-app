import mealRoutes from './meals.routes';
import categoriesRoutes from './categories.routes';

export default [
	...mealRoutes,
	...categoriesRoutes,
];
