import * as constants from '../constants';
import { Meals } from '../components/pages';
import Layout from '../components/organisms/layout/layout.organism';

export default [
	{
		path: constants.routes.meals.LIST,
		component: Meals,
		useLayout: true,
		layout: Layout,
		props: {},
		requireLogin: true,
	},
];
