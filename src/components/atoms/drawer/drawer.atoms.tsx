/* eslint-disable no-lone-blocks */
import * as React from 'react';
import { Toolbar, IconButton, Typography } from '@mui/material';

import { Link } from 'react-router-dom';

import { Menu } from '@mui/icons-material';
import { connect } from 'react-redux';

// import { menuItems } from '../../../data';
import * as interfaces from '../../../interfaces';

function mapStateToProps(state: interfaces.redux.Store) {
	return {
		user: state.user,
	};
}

export default connect(mapStateToProps, null)(({
	_open, handleDrawerOpen, user,
}: any) => {
	const brand = user.email;
	return (
		<Toolbar>
			<IconButton
				color="inherit"
				aria-label="open"
				onClick={handleDrawerOpen}
			>
				<Menu />
			</IconButton>
			<Typography className="text-center w-100p ml-40-reverse" variant="h6" noWrap component="div">
				<Link className="text-decoration-none text-white" to="/">{brand}</Link>
			</Typography>
		</Toolbar>
	);
});
