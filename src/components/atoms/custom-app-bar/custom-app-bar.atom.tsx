import * as React from 'react';
// import { Hidden } from '@mui/material';

import { AppBar } from '../../../styles/react/drawer.styles';
import CustomToolbar from '../drawer/drawer.atoms';

export default ({ open, handleDrawerOpen }: any) => {
	return (
		<AppBar key="appbar-2" open={open}>
			<CustomToolbar key="toolbar-2" handleDrawerOpen={handleDrawerOpen} />
		</AppBar>
	);
};
