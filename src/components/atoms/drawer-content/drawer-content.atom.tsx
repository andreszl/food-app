import * as React from 'react';
import { useTheme } from '@mui/material/styles';
import { List, Divider, IconButton, ListItem, ListItemIcon, ListItemText } from '@mui/material';
import { Link } from 'react-router-dom';

import { ChevronLeft, ChevronRight, Logout, Category } from '@mui/icons-material';
import { useDispatch } from 'react-redux';

import { DrawerHeader } from '../../../styles/react/drawer.styles';
import { logout } from '../../../actions/auth/auth.actions';
import * as constants from '../../../constants';

export default ({ handleDrawerClose }: any) => {
	const dispatch = useDispatch();
	const theme = useTheme();

	return (
		<>
			<DrawerHeader>
				<IconButton onClick={handleDrawerClose}>
					{theme.direction === 'ltr' ? <ChevronLeft /> : <ChevronRight />}
				</IconButton>
			</DrawerHeader>
			<Divider />
			<List>
				<ListItem button>
					<Link className="custom-link d-flex" to={constants.routes.categories.LIST}>
						<ListItemIcon>
							<Category />
						</ListItemIcon>
						<ListItemText primary="Categorias" />
					</Link>
				</ListItem>
				<ListItem button onClick={() => dispatch(logout() as any)}>
					<ListItemIcon>
						<Logout />
					</ListItemIcon>
					<ListItemText primary="Cerrar Sesion" />
				</ListItem>
			</List>
		</>
	);
};
