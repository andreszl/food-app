import Meals from './meals.page';
import Categories from './categories.page';

export {
	Meals,
	Categories,
};
