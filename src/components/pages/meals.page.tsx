import React, { useEffect, useState } from 'react';
import Zoom from 'react-medium-image-zoom';
import 'react-medium-image-zoom/dist/styles.css';
import Paper from '@mui/material/Paper';
import InputBase from '@mui/material/InputBase';
import IconButton from '@mui/material/IconButton';
import SearchIcon from '@mui/icons-material/Search';
import { useDispatch } from 'react-redux';
import { Card, CardActions, CardContent, Chip, Typography } from '@mui/material';
import { StarBorderOutlined, Grade, FavoriteBorder, Favorite } from '@mui/icons-material';
import _ from 'lodash';
import { connect } from 'react-redux';

import * as interfaces from '../../interfaces';
import api from '../../api';
import repository from '../../repository';
import actions from '../../actions';

const PAGE_SIZE = 5;

function mapStateToProps(state: interfaces.redux.Store) {
	return {
		user: state.user,
		category: state.categories,
	};
}

const { reset } = actions.categories;

const mapDispatchToProps = {
	reset,
};

type MapStateToProps = ReturnType<typeof mapStateToProps>;
type MapDispatchToProps = typeof mapDispatchToProps;
interface Props extends MapStateToProps, MapDispatchToProps { }

export default connect(mapStateToProps, mapDispatchToProps)((props: Props) => {
	const [meals, setMeals] = useState<interfaces.meals.Meal[]>([]);
	const [ratings, setRatings] = useState<interfaces.ratings.IRating[]>([]);
	const [currentPage, setCurrentPage] = useState(1);
	const [search, setSearch] = useState('');
	const [tags, setTags] = useState<interfaces.meals.Meal[]>([]);
	const dispatch = useDispatch();

	useEffect(() => {
		const { category, reset } = props;
		if (category.strCategory !== '') {
			getByCategory();
		} else {
			getMeals();
		}
		getHistory();

		return () => {
			dispatch(reset());
		};
	}, []);

	const handlePageChange = (pageNumber: number) => {
		setCurrentPage(pageNumber);
	};

	const getByCategory = async () => {
		const { category } = props;
		const data = await api.meals.getByCategory(category.strCategory);
		setMeals(data.meals);
		getRatings(data.meals);
	};

	const getMeals = async () => {
		const data = await api.meals.getByArea('Canadian');
		setMeals(data.meals);
		getRatings(data.meals);
	};

	const getByName = async () => {
		const { user } = props;
		const data = await api.meals.getByName(search);
		if (data.meals.length > 0) {
			setMeals([data.meals[0]]);
			getRatings([data.meals[0]]);
			repository.history.add({ userId: user._id, search, ...data.meals[0] });
		}
	};

	const rate = async (
		stars: number,
		mealId: string,
		like: boolean,
		rating: interfaces.ratings.IRating|undefined,
	) => {
		const { user } = props;
		if (rating) {
			repository.ratings.save(rating._id, { userId: user._id, mealId, stars, like });
		} else {
			repository.ratings.add({ userId: user._id, mealId, stars, like });
		}

	};

	const getRatings = async (mealsData: interfaces.meals.Meal[]) => {
		const { user } = props;
		repository.ratings.getByMeaIdslAndUserId<any>(
			mealsData.map((m) => m.idMeal),
			user._id,
			(snapshot) => {
				const data = snapshot.docs.map((doc) => {
					return { ...doc.data(), _id: doc.id };
				});

				setRatings(data);
			},
		);
	};

	const getHistory = async () => {
		const { user } = props;
		repository.history.getByUserId<any>(
			user._id,
			(snapshot) => {
				const data = snapshot.docs.map((doc) => {
					return { ...doc.data(), _id: doc.id };
				});

				setTags(data);
			},
		);
	};

	const paginatedMeals = meals.slice((currentPage - 1) * PAGE_SIZE, currentPage * PAGE_SIZE);

	return (
		<div className="w-100p d-flex justify-content-center">
			<div className="w-100p max-width-600">
				<div className="">
					<Paper
						component="form"
						sx={{ p: '2px 4px', display: 'flex', alignItems: 'center', width: '100%' }}
					>
						<InputBase
							value={search}
							onChange={(e: any) => setSearch(e.target.value)}
							sx={{ ml: 1, flex: 1 }}
							placeholder="Buscar..."
							inputProps={{ 'aria-label': 'Buscar' }}
						/>
						<IconButton onClick={() => getByName()} type="button" sx={{ p: '10px' }} aria-label="search">
							<SearchIcon />
						</IconButton>
					</Paper>
				</div>
				<div className="">
					{
						tags.map((tag, index) => {
							return (
								<Chip key={`tag-${index}-${Math.random().toString(36).substring(7)}`} className="mr-4 mt-12" label={tag.search} />
							);
						})
					}
				</div>
				{
					paginatedMeals.map((meal, index) => {
						const rating = _.find(ratings, { mealId: meal.idMeal }) || undefined;
						return (
							<div key={`meal-${index}-${Math.random().toString(36).substring(7)}`} data-testid="meal-item">
								<Card className="w-100p mt-12">
									<CardContent>
										<div className="d-flex p-7">
											<div className="p-0 m-0">
												<Zoom>
													<img className="br-12" width={100} src={meal.strMealThumb} alt="" />
												</Zoom>
											</div>
											<div className="ml-14">
												<Typography className="bold-900 text-black" variant="body2" color="text.secondary">
													{meal.strMeal}
												</Typography>
												<div className="d-flex">
													{Array.from({ length: 5 }, (_, index) => (
														<div key={`star-${index}-${Math.random().toString(36).substring(7)}`} className="d-flex">
															{
																rating && rating.stars >= index + 1 ? (
																	<Grade
																		onClick={
																			() => rate(
																				index + 1,
																				meal.idMeal,
																				rating ? rating.like : false,
																				rating,
																			)
																		}
																	/>
																) : (
																	<StarBorderOutlined
																		onClick={
																			() => rate(
																				index + 1,
																				meal.idMeal,
																				rating ? rating.like : false,
																				rating,
																			)
																		}
																	/>
																)
															}
														</div>
													))}
												</div>
											</div>
										</div>
									</CardContent>
									<CardActions className="p-0 m-0 position-relative">
										<div />
										<div>
											{
												rating && rating.like ? (
													<Favorite onClick={() => rate(rating ? rating.stars : 0, meal.idMeal, false, rating)} className="position-absolute right-0 mt-28-reverse mr-8" />
												) : (
													<FavoriteBorder onClick={() => rate(rating ? rating.stars : 0, meal.idMeal, true, rating)} className="position-absolute right-0 mt-28-reverse mr-8" />
												)
											}
										</div>
									</CardActions>
								</Card>
							</div>
						);
					})
				}
				<div className="pagination w-100p d-flex justify-content-center align-items-center mt-12">
					{Array.from({ length: Math.ceil(meals.length / PAGE_SIZE) }, (_, index) => (
						<button
							key={`page-${index}`}
							onClick={() => handlePageChange(index + 1)}
							className={`${currentPage === index + 1 ? 'page-active' : ''}`}
						>
							{index + 1}
						</button>
					))}
				</div>
			</div>
		</div>
	);
});
