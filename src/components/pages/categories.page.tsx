import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Card, CardContent, Typography } from '@mui/material';
import { Link } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import Zoom from 'react-medium-image-zoom';
import { ArrowForwardIos } from '@mui/icons-material';

import 'react-medium-image-zoom/dist/styles.css';
import * as interfaces from '../../interfaces';
import actions from '../../actions';
import * as constants from '../../constants';
import api from '../../api';

function mapStateToProps(state: interfaces.redux.Store) {
	return {
		user: state.user,
	};
}

const { setCategory } = actions.categories;

const mapDispatchToProps = {
	setCategory,
};

type MapStateToProps = ReturnType<typeof mapStateToProps>;
type MapDispatchToProps = typeof mapDispatchToProps;

interface Props extends MapStateToProps, MapDispatchToProps { }

export default connect(mapStateToProps, mapDispatchToProps)((props: Props) => {
	const [categories, setCategories] = useState<any[]>([]);
	const dispatch = useDispatch();

	useEffect(() => {
		getCategories();
	}, []);

	const getCategories = async () => {
		const data = await api.categories.get();
		setCategories(data.categories);
	};

	const selectCategory = (category: any) => {
		dispatch(setCategory(category));
	};

	const { setCategory } = props;

	return (
		<div className="w-100p d-flex justify-content-center">
			<div className="w-100p max-width-600">
				{
					categories.map((category, index) => {
						return (
							<Card
								key={`category-${index}-${Math.random().toString(36).substring(7)}`}
								className="w-100p mt-12"
							>
								<CardContent>
									<div className="d-flex p-7 justify-content-between align-items-center">
										<div className="d-flex">
											<Zoom>
												<img className="br-12" width={100} src={category.strCategoryThumb} alt="" />
											</Zoom>
											<Typography className="bold-900 text-black pl-12" variant="body2" color="text.secondary">
												{category.strCategory}
											</Typography>
										</div>
										<Link className="custom-link" to={constants.routes.meals.LIST}>
											<ArrowForwardIos className="cursor-pointer" onClick={() => selectCategory(category)} />
										</Link>
									</div>

								</CardContent>
							</Card>
						);
					})
				}
			</div>
		</div>
	);
});
