import request from '../../utils/request.util';

export async function get() {
	return request({
		url: 'api/json/v1/1/categories.php',
		method: 'GET',
	});
}
