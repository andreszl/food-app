import * as meals from './meals/meals.api';
import * as categories from './categories/categories.api';

const api = {
	meals,
	categories,
};

export default api;
