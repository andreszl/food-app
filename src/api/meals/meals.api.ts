import request from '../../utils/request.util';

export async function getByArea(area: string) {
	return request({
		url: `api/json/v1/1/filter.php?a=${area}`,
		method: 'GET',
	});
}

export async function getByName(name: string) {
	return request({
		url: `api/json/v1/1/search.php?s=${name}`,
		method: 'GET',
	});
}

export async function getByCategory(category: string) {
	return request({
		url: `api/json/v1/1/filter.php?c=${category}`,
		method: 'GET',
	});
}
