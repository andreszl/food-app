import { combineReducers } from 'redux';
import user from './auth/auth.reducer';
import categories from './categories.reducer';

const reducers = combineReducers({
	user,
	categories,
});

export default reducers;
