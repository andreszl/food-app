import * as constants from '../constants';
import * as interfaces from '../interfaces';
import { IActions } from '../actions/categories.actions';

export default (
	state: interfaces.categories.ICategory = interfaces.categories.initialState,
	action: IActions,
) => {
	switch (action.type) {
		case constants.categories.actions.SET:
			return {
				...state,
				strCategory: action.payload.strCategory,
			};

		case constants.categories.actions.RESET:
			return {
				...state,
				strCategory: '',
			};

		default:
			return state;
	}
};
