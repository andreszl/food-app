export * as routes from './routes.interfaces';
export * as meals from './meals.interfaces';
export * as ratings from './ratings.interfaces';
export * as validations from './validations.interfaces';
export * as request from './request.interfaces';
export * as redux from './redux.interfaces';
export * as auth from './auth.interfaces';
export * as categories from './categories.interfaces';
