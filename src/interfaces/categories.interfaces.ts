export interface ICategory {
	strCategory: string
}

export const initialState: ICategory = {
	strCategory: '',
};
