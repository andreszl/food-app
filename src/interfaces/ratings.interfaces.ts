export interface IRating {
	like: boolean;
	_id: string;
	mealId: string;
	stars: number;
	userId: string;
}
