import * as ratings from './ratings.dao';
import * as history from './history.dao';

export default {
	ratings,
	history,
};
