import { QuerySnapshot, collection, doc, onSnapshot, query, setDoc, where, limit } from 'firebase/firestore';
import ObjectID from 'bson-objectid';
import { db } from './firestore.config';

export async function add(data: any) {
	setDoc(doc(db, 'history', ObjectID().toHexString()), data);
}

export const getByUserId = async <T>(
	userId: string, cb: (_data: QuerySnapshot<T>) => void,
) => {
	const q = query(query(query(collection(db, 'history'), where('userId', '==', userId), limit(10))));
	return onSnapshot(q, (snapshot) => {
		cb(snapshot as QuerySnapshot<T>);
	});
};
