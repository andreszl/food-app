import { QuerySnapshot, collection, doc, onSnapshot, query, setDoc, where } from 'firebase/firestore';
import ObjectID from 'bson-objectid';
import { db } from './firestore.config';

export async function save(id: any, data: any) {
	return setDoc(doc(db, 'ratings', id), data);
}

export const getByMeaIdslAndUserId = async <T>(
	mealIds: string[], userId: string, cb: (_data: QuerySnapshot<T>) => void,
) => {
	const q = query(query(query(collection(db, 'ratings'), where('userId', '==', userId)), where('mealId', 'in', mealIds)));
	return onSnapshot(q, (snapshot) => {
		cb(snapshot as QuerySnapshot<T>);
	});
};

export async function add(data: any) {
	setDoc(doc(db, 'ratings', ObjectID().toHexString()), data);
}
