import { initializeApp } from 'firebase/app';
import { getFirestore } from 'firebase/firestore';
import { getAuth } from 'firebase/auth';
import 'firebase/storage';

const firebaseConfig = {
	apiKey: 'AIzaSyDDKObOjy2pc3bKStkUPXLc3Yfbq5qD9vw',
	authDomain: 'food-app-8c88f.firebaseapp.com',
	projectId: 'food-app-8c88f',
	storageBucket: 'food-app-8c88f.appspot.com',
	messagingSenderId: '511223064164',
	appId: '1:511223064164:web:6f45ef2b7aa896c822e171',
};

export const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export const firebaseAdmin = app;
export const db = getFirestore(app);
