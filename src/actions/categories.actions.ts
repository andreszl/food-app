import * as constants from '../constants';
import * as interfaces from '../interfaces';

export const setCategory = (payload: interfaces.categories.ICategory) => {
	return {
		type: constants.categories.actions.SET,
		payload,
	} as const;
};

export const reset = () => {
	return {
		type: constants.categories.actions.RESET,
	} as const;
};

const actions = {
	setCategory,
	reset,
};

export type IActions = interfaces.redux.IActionUnion<typeof actions>;
