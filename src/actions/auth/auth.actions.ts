import { onAuthStateChanged, signOut } from 'firebase/auth';
import 'firebase/auth';
import * as constants from '../../constants';
import * as interfaces from '../../interfaces';
import { decode } from '../../utils/jwt.util';
import { auth } from '../../repository/firestore.config';

export const setCurrentUser = (payload: interfaces.auth.IUser) => {
	return {
		type: constants.auth.actions.SET_CURRENT_USER,
		payload,
	} as const;
};

export function signIn() {
	return (async (dispatch: interfaces.redux.IDistpatch, _getState: interfaces.redux.IGetState) => {
		try {
			const credentials: any = await new Promise((resolve, reject) => {
				onAuthStateChanged(auth, ((user: any) => {
					if (user) {
						resolve(user);
						console.log('onAuthStateChanged', user);
					}
				}), (err) => {
					if (err) {
						reject(err);
					}
				});
			});

			const token = localStorage.getItem('accessToken') as string;
			const decoded: any = decode(token);

			dispatch(
				setCurrentUser({
					_id: decoded.user_id,
					email: decoded.email,
					roles: ['user'],
					iat: decoded.iat,
					exp: decoded.exp,
					accessToken: credentials.accessToken,
				}),
			);

			return true;
		} catch (err: any) {
			logout();
			console.log(err);
			return false;
		}
	}) as unknown as Promise<boolean>;
}

export function logout() {
	return (async (dispatch: interfaces.redux.IDistpatch, _getState: interfaces.redux.IGetState) => {
		try {
			localStorage.removeItem('accessToken');

			dispatch(
				setCurrentUser({
					_id: '',
					email: '',
					roles: [],
					iat: 0,
					exp: 0,
					accessToken: '',
				}),
			);

			signOut(auth);
			return { message: 'success' };
		} catch (err: any) {
			return { message: err.message };
		}
	}) as unknown as Promise<{ message: string}>;
}

const actions = {
	setCurrentUser,
};

export type IActions = interfaces.redux.IActionUnion<typeof actions>;
