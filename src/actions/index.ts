import * as auth from './auth/auth.actions';
import * as categories from './categories.actions';

export default {
	auth,
	categories,
};
