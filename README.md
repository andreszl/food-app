# Mi Aplicación en React y TypeScript

Esta es una aplicación en React y TypeScript que utiliza la URL de Heroku [https://cors-anywhere.herokuapp.com/corsdemo](https://cors-anywhere.herokuapp.com/corsdemo) para permitir llamadas a servicios externos siempre y cuando se este en desarrollo, de lo contrario debe eliminar la url de heroku si se desea usar en un entorno productivo

## Habilitar Heroku

* Ve a la siguiente url: https://cors-anywhere.herokuapp.com/corsdemo
* Click en el boton: "Request temporary access to the demo server"

![Alt text](./docs/image.png)


una vez hecho lo anterior heroku nos permitira un cantidad gratis de solicitudes que podemos hacer a apis externas

## Instalación

Para instalar las dependencias de la aplicación, ejecuta el siguiente comando:

```shell
npm install --legacy-peer-deps
```

## Variables de Entorno para desarrollo
La aplicación utiliza las siguientes:

* FAST_REFRESH=false: Esta variable desactiva la función de actualización rápida de React.
* HOKIDAR_USEPOLLING=true: Esta variable habilita el uso de polling para la detección de cambios en los archivos.
* REACT_APP_API_URL: Esta variable contiene la URL de la API que se utilizará para realizar las llamadas a los servicios. En este caso, se utiliza la URL https://cors-anywhere.herokuapp.com/https://www.themealdb.com.
* REACT_APP_ENV=dev: Esta variable indica el entorno de desarrollo de la aplicación.

## Uso

Para ejecutar la aplicación en modo de desarrollo, utiliza el siguiente comando:

```shell
	npm install --legacy-peer-deps
```


Esto iniciará la aplicación en el navegador y podrás verla en http://localhost:3000.

## editorconfig

Se esta implementando un .editorconfig, si instalas el plugin de vscode llamado: EditorConfig for VS Code inmediatamente te aplicara esas configuraciones


## ESLint

Se esta implementando un .eslintrc, si instalas el plugin de vscode llamado: ESLint inmediatamente te aplicara esas configuraciones



## Nota IMPORTANTE: 

La aplicacion esta con firebase y ya que la configuracion esta directamente en mi archivo firebase no hay necesidad de mas nada ya que desde el front se conecta directamente a la base de datos por otro lado tanto el iniciar sesion como el registrarse se encuentran tambien con firebase

Acontinuacion unas imagenes de muestra: 

![Alt text](./docs/image-1.png)

![Alt text](./docs/image-2.png)

![Alt text](./docs/image-3.png)

